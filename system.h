#ifndef _SYSTEM_H_
#define _SYSTEM_H_

// include the library header
#include "library.h"
// include list of modules used
#include "task.h"
#include "timing.h"
#include "list.h"
#include "buffer.h"
#include "buffer_printf.h"
#include "charReceiverList.h"
#include "itemBuffer.h"
#include "uart.h"
#include "game_MUH3.h"
#include "game.h"
#include "subsys.h"
#include "terminal.h"
#include "hal_general.h"
#include "game_DanBlaze.h"

//hint: the MSP430F5529 uses UART1 for the builtin MSP Application UART1 virtual COM port
#define USE_UART0

#define SUBSYS_UART 0
#define FCPU     1100000
// if peripheral clock is slower than main clock change it here
#define PERIPHERAL_CLOCK FCPU

#endif // _SYSTEM_H_
