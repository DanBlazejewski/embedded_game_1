/*
 * game_DanBlaze.c
 *
 *  Created on: Mar 26, 2015
 *      Author: Dan
 */

#include "system.h"
#include "random_int.h"
#include "strings.h"

#define MAP_WIDTH 40
#define MAP_HEIGHT 30


struct DanBlaze_game_t
{
	char x; // X coordinate of gun
	int score; //how many ships were killed
	uint8_t id; //ID of game
};
struct  DanBlaze_game_t game;

enum game_states {
	IDLE, START
};

static void Callback(int argc, char * argv[]);
static void Receiver(char c);

static void PrePlay(void);
static void Play(void);
static void Help(void);

void DanBlaze_Init()
{
	game.id = Game_Register("DBG", "Galaga", PrePlay, Help);
}

void PrePlay(void)
{
	Game_Printf("Welcome to Galaga!");
	Task_Schedule(Play, 0, 1000, 0);
}

void Play(void)
{
	Game_ClearScreen();
	Game_DrawRect(0,0,MAP_WIDTH, MAP_HEIGHT);
}

void Help(void)
{
	Game_Printf("Lol good luck");

}

