#include <msp430.h> 

#include <stdio.h>
#include <stdlib.h>
#include "system.h"
//#include <plib.h>
//#include "library.h"
#include "hal_general.h"

//#include <xc.h>

/*
 * main.c
 */
#define UART_CHANNEL 0
void hello_world(void)
{
	UART_Printf(UART_CHANNEL, "Hello World\r\n");
}
int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
	
    Timing_Init();
    Task_Init();
    UART_Init(0);
    SystemInit();
    Game_Init();
    MUH3_Init();
    DanBlaze_Init();
    EnableInterrupts();
    //Task_Schedule(hello_world, 0, 2000, 2000);
    while(1) SystemTick();
	return 0;
}
